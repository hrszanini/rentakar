var http = require("http").createServer(initServer);
var fs = require("fs");
var port = 443;

function initServer(requisicao, resposta){
  var url = requisicao.url;
  var arq;
  if(url == '/')
    url = "index.html";

  console.log("Path: "+requisicao.url);

  try{
    arq = fs.readFileSync("../front/"+url);
    resposta.writeHead(200);
  }catch(e){
    arq = null;
    resposta.writeHead(404);
  }finally{
    resposta.end(arq);
  }

};

http.listen(port, function(){
  console.log("Server Online on port "+port);
});
