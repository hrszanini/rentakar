function criarLogin(){
  var email = document.getElementById('eMail').value;
  var name = document.getElementById('fName').value+" "+document.getElementById('lName').value;
  var pass = document.getElementById('pass').value;
  var passConf = document.getElementById('passConf').value;

  if(!validarEmail(email)){
    alert('Insira um e-mail válido');
    return false;
  }
  if(pass != passConf){
    alert('As senhas não conferem');
    return false;
  }
  if(localStorage.getItem(email) == null){
    localStorage.setItem(email,pass);
    localStorage.setItem(email+"_name",name);
    alert("Cadastrado com sucesso!");
    return true;
  }else{
    alert('E-mail já cadastrado');
    return false;
  }
}

function data(){
  var data = new Date;
  var res = data.getDate()+"/"+(data.getMonth()+1)+"/"+data.getFullYear();
  return res;
}

function alterarLetra(tamanho){
  return function() {
    document.getElementById("logo").style.fontSize = tamanho + "px";
  };
}

clos1 = alterarLetra("50");
clos2 = alterarLetra("75");
clos3 = alterarLetra("100");

function validarEmail(email){
  if(email.indexOf("@") <= 0){
    return false;
  }
  if(email.indexOf("@") != email.lastIndexOf("@")){
    return false;
  }
  if(email.lastIndexOf(".") < email.indexOf("@")){
    return false;
  }
  if(email.length <= (email.lastIndexOf(".")+1)){
    return false;
  }
  return true;
}

function validarLogin(user,pass){
  var email = document.getElementById(user).value;
  var password = document.getElementById(pass).value;
  const val = localStorage.getItem(email);
  if(val != null){
    if(val == password){
      localStorage.getItem(email+"_name");
      localStorage.setItem("user",email);
      location.href="main.html";
    }else{
      alert('Senha incorreta!');
    }
  }else{
    alert('Email não cadastrado!');
  }
}

function changeWorkspace(nome){
  document.getElementById('Workspace').innerHTML="<iframe src='./"+nome+".html'</iframe>";
}

function mainLoad(){
  if(localStorage.getItem('user')==''){
    location.href='index.html';
  }
  document.getElementById('welcome').innerHTML='Olá, '+localStorage.getItem(localStorage.getItem('user')+'_name');
}

function carroSelec(){
  if(document.getElementById('celta').checked)
    return 'celta';
  if(document.getElementById('gol').checked)
    return 'gol';
  if(document.getElementById('corsa').checked)
    return 'corsa';
  return null;
}

function pedido(dataRet, dataEnt){
  var data_ret = document.getElementById(dataRet).value;
  var data_ent = document.getElementById(dataEnt).value;
  var carro = carroSelec();
  var user = localStorage.getItem("user");

  if(data_ret == ""){
    alert("Escolha a data de retirada.");
    return null;
  }
  if(data_ret == ""){
      alert("Escolha a data de entrega.");
      return null;
  }
  var msg = "Pedido realizado com sucesso!";
  if(data_ret <= data_ent){
    if(localStorage.getItem(user+"_carro") != null){
      msg = "Pedido atualizado com sucesso!";
    }
    localStorage.setItem(user+"_dataRet",data_ret);
    localStorage.setItem(user+"_dataEnt",data_ent);
    localStorage.setItem(user+"_carro",carro);
    alert(msg);
  }else{
    alert("A data da retirada deve ser igual ou anterior a data de entrega.");
  }
}

function consultar(){
  var data_ret = document.getElementById('dataRet');
  var data_ent = document.getElementById('dataEnt');
  var carro = document.getElementById('carro');
  var user = localStorage.getItem("user");

   if(localStorage.getItem(user+"_dataRet") == null){
     document.getElementById("visualiza").style.visibility = "hidden";
   }

   data_ret.value = localStorage.getItem(user+"_dataRet");
   data_ent.value = localStorage.getItem(user+"_dataEnt");;
   carro.src = converteCarro(localStorage.getItem(user+"_carro"));
}

function converteCarro(carro){
  if(carro == "celta")
    return "img/celta.jpg";
  if(carro == "gol")
    return "img/gol.png";
  if(carro == "corsa")
    return "img/corsa.jpg";
  return "";
}
